import numpy as np
import random

"""
@author vcoindet
Jeu de memory
"""

Tabl = ['a', 'a', 'b', 'b', 'c', 'c', 'd', 'd', 'e', 'e', 'f', 'f']

def melange_carte(Tab):
    """
        Fonction qui permet de mélanger les cartes
        :param Tab: cartes à mélanger dans une liste
        :type Tab: list
        :return: retourne la liste de cartes melangees
        :rtype: list
    """
    for i in range(1, len(Tab)):
        # pick an element in Tab[:i+1] with which to exchange Tab[i]
        j = int(random.random() * (i+1))
        Tab[i], Tab[j] = Tab[j], Tab[i]
    return Tab

def carte_cache(Tab):
    """
        Doit créer une liste de même longueur que la liste renvoyée dans la question précédente
        :param Tab: liste de cartes 
        :type Tab: list
        :return: une nouvelle liste de cartes
        :rtype: list
    """
    return np.zeros(len(Tab))


def choisir_cartes(Tab):
    """
        Fait choisir deux cartes de la liste sans que les deux soient les memes
        :param Tab: liste de cartes
        :type Tab: list
        :return: les deux cartes choisies sous forme de liste de deux entiers
        :rtype: list
    """
    c1 = int(input("Choisissez une carte : "))
    print(Tab[c1])
    c2 = int(input("Choisissez une deuxieme carte : "))
    while c1 == c2:
        print("Erreur, la deuxieme carte ne peut être la même que la premiere ! ")
        c2 = int(input("Veuillez choisir une deuxieme carte différente de la premiere : "))
    print(Tab[c2])
    return [c1,c2]


def retourne_carte (c1, c2, Tab, Tab_cache):
    """
        Doit retrouner les cartes dans la liste cachée
        :param c1: l'indice de la carte c1
        :type c1: int
        :param c2: l'indice de la carte c2
        :type c2: int
        :param Tab: liste de cartes
        :type Tab: list
        :param Tab_cache: liste de cartes cachees
        :type Tab_cache: list
        :return: la liste Tab_cache mais avec les deux cartes retournees
        :rtype: list
    """
    Tab_cache[c1] = Tab_cache[c1]
    Tab_cache[c2] = Tab_cache[c2]
    return Tab_cache

def jouer(Tab):
    """
        Lance le jeu
        :param Tab: liste de cartes
        :type Tab: list
    """
    print("Bienvenue sur notre Jeu Memory, ci dessous voici les cartes mise en jeu. Il y en a 12")
    print(Tab)
    Tab = melange_carte(Tab)
    print("Maintenant je vais melanger les cartes et les retourner")
    
    Tab_cache =  carte_cache(Tab)
    print(Tab_cache)

    [c1, c2] = choisir_cartes(Tab)
    Tab_cache = retourne_carte(c1, c2, Tab, Tab_cache)
    print(Tab_cache)
    
    while 0 in Tab_cache:
        [c1, c2] = choisir_cartes(Tab)
        
        Tab_cache = retourne_carte(c1, c2, Tab, Tab_cache)
        
        print(Tab_cache)
        
    print("Bravo tu as gagné!!!")

    
jouer(Tabl)
